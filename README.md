# Java Maven 3 Project Template

![Made with Java](https://forthebadge.com/images/badges/made-with-java.svg) ![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/Skerwe/java-maven-project-template?style=for-the-badge)

A basic Apache Maven 3 Java project template with reporting and documentation:

- Javadocs for source files
- Surefire unit test reports
- Jacoco code coverage reports
- PMD and Checkstyle code quality reports

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Make sure these are installed first.

- You will need Java 1.8 installed and configured on your system PATH.  
  It's best to use the [OpenJDK][openjdk], Windows users can get binaries from [AdoptOpenJDK][adoptopenjdk]
- [Apache Maven 3+][maven]

### Installing and Running

1. Clone the repository with the name of your new project:  
   `git clone https://Skerwe@bitbucket.org/Skerwe/java-maven-project-template.git <project-name>`

2. Compile, test and bundle the application:  
   `mvn compile package`

3. Execute the application:  
   `java -cp target/maven-project-template-1.0-SNAPSHOT.jar za.web.skerwe.code.template.App`

## The Maven Site and Report plugins

Access the Maven dashboard site on [localhost:8080](http://localhost:8080/)

```shell
mvn site site:run
```

The site will include all other reports (the test reports, code coverage and code quality reports).  
But you can run each report separatly:

### 1. Surefire Test Reports

```shell
mvn surefire-report:report
```

Test report output to folder:

`target\site\surefire-reports`

### 2. Javadocs

```shell
mvn javadoc:javadoc
```

Javadocs output folder:

`target\site\apidocs`

### 3. Code Coverage

```shell
mvn jacoco:check
```

### 4. Code Quality

```shell
mvn checkstyle:check
```

```shell
mvn pmd:check
```

## This project was built with

- [Java](https://www.java.com/en/) programming language
- [Maven][maven] build tool
- [JUnit Jupiter][junit] (JUnit 5) testing framework

## License

The source code is free -- see the [UNLICENSE](UNLICENSE) file for details

[openjdk]: https://openjdk.java.net/
[adoptopenjdk]: https://adoptopenjdk.net/
[maven]: https://maven.apache.org/
[junit]: https://junit.org/junit5/
