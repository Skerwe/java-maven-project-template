package za.web.skerwe.code.template;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AppTest {

  @Test
  public void shouldAnswerWithTrue() {
    App classUnderTest = new App();
    assertTrue(classUnderTest.getMessage().equals("Hello World!"));
  }
}
